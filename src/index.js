import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import api from './middleware/api';
import loginApp from './reducers';

let createStoreWithMiddleware = applyMiddleware(reduxThunk, api)(createStore);

let store = createStoreWithMiddleware(loginApp);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root'));
registerServiceWorker();
