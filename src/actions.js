export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE';

const USERNAME = 'user';
const PASSWORD = 'password';

// mock login request
function mockLogin(credentials) {
    return new Promise((resolve, reject) => setTimeout(() => {
        if (credentials.username === USERNAME && credentials.password === PASSWORD) {
            resolve({id_token: 'Some random string...'})
        } else {
            reject({message: 'Mali yung credentials'});
        }
    }, 3000));
}

function requestLogin(credentials) {
    return {
        type: LOGIN_REQUEST,
        isFetching: true,
        isLoggedIn: false,
        credentials,
    };
}

function requestLogout() {
    return {
        type: LOGOUT_REQUEST,
        isFetching: true,
        isLoggedIn: true,
    };
}

function receiveLogin(user) {
    return {
        type: LOGIN_SUCCESS,
        isFetching: false,
        isLoggedIn: true,
        id_token: user.id_token,
    };
}

function receiveLogout() {
    return {
        type: LOGOUT_SUCCESS,
        isFetching: false,
        isLoggedIn: false,
    };
}

function loginError(message) {
    return {
        type: LOGIN_FAILURE,
        isFetching: false,
        isLoggedIn: false,
        message,
    };
}

export function loginUser(credentials) {
    return (dispatch) => {
        dispatch(requestLogin(credentials));

        return fetch('https://myapi.dev/api/oauth', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(credentials),
        }).then((response) => response.json())
        .then((user) => {
            localStorage.setItem('id_token', user.id_token);
            dispatch(receiveLogin(user));
        }).catch((err) => {
            console.log('Error: ', err);
            dispatch(loginError(err.message));
        });
    };
}

export function logoutUser() {
    return (dispatch) => {
        dispatch(requestLogout());
        localStorage.removeItem('id_token');
        dispatch(receiveLogout());
    };
}
