import { combineReducers } from 'redux';

import { 
    LOGIN_REQUEST, 
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_SUCCESS,
    LOGOUT_REQUEST,
} from './actions';

const initialState = {
    isFetching: false,
    isLoggedIn: !!localStorage.getItem('id_token')
};

function auth(state = initialState, action) {
    switch(action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                isFetching: true,
                isLoggedIn: false,
                user: action.credentials,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isLoggedIn: true,
                errorMessage: '',
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                isFetching: false,
                isLoggedIn: false,
                errorMessage: action.message
            };
        case LOGOUT_REQUEST:
            return {
                ...state,
                isFetching: true,
                isLoggedIn: false,
            };
        case LOGOUT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isLoggedIn: false,
            };
        default:
            return state;
    }
}

const loginApp = combineReducers({ auth });

export default loginApp;
