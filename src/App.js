import React from 'react';
import { connect } from 'react-redux';
import './App.css';
import MainView from './components/MainView';

const App = ({dispatch, isLoggedIn, errorMessage}) => (
  <div>
    <MainView 
      isLoggedIn={isLoggedIn}
      errorMessage={errorMessage}
      dispatch={dispatch}
    />
  </div>
);

function mapStateToProps(state) {
  const { auth } = state;
  const { isLoggedIn, errorMessage } = auth;

  return {
    isLoggedIn, errorMessage
  };
}

export default connect(mapStateToProps)(App);
