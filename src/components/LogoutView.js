import React from 'react';

const LogoutView = ({onLogoutClick}) => (
    <button onClick={() => onLogoutClick()}>
        Logout
    </button>
);

export default LogoutView;
