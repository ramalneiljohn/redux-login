import React from 'react';
import { loginUser, logoutUser } from '../actions';
import LoginView from './LoginView';
import LogoutView from './LogoutView';

const MainView = ({dispatch, isLoggedIn, errorMessage}) => (
  <div>
    {!isLoggedIn &&
      <LoginView 
        errorMessage={errorMessage}
        onLoginClick={(credentials) => dispatch(loginUser(credentials))}
      />
    }
    {isLoggedIn &&
      <LogoutView onLogoutClick={() => dispatch(logoutUser())} />
    }
  </div>
);

export default MainView;
