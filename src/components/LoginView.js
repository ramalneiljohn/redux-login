import React, { Component } from 'react';

class LoginView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        const username = this.state.username.trim();
        const password = this.state.password.trim();

        const credentials = { username, password };

        console.log(credentials);

        this.props.onLoginClick(credentials);
    }

    render() {
        const { errorMessage } = this.props;

        return (
            <div>
                <input type="text" placeholder="Username" 
                    onInput={(event) => this.setState({ username: event.target.value })} />
                <input type="password" placeholder="Password" 
                    onInput={(event) => this.setState({ password: event.target.value })} />
                <button onClick={this.handleClick}>Login</button>

                {errorMessage &&
                    <p>{errorMessage}</p>
                }
            </div>
        );
    }
}

export default LoginView;
